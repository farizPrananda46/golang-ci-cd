package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/xvbnm48/gin-course/entity"
)

const (
	TITLE       = "video title"
	DESCRIPTION = "video description"
	URL         = "https://www.youtube.com/embed/ganw5GJAeRE"
)

func getVideo() entity.Video {
	return entity.Video{
		Title:       TITLE,
		Description: DESCRIPTION,
		URL:         URL,
	}
}

func TestFindAll(t *testing.T) {
	service := New()

	service.Save(getVideo())

	videos := service.FindAll()

	firstVideo := videos[0]
	assert.Nil(t, videos)
	assert.Equal(t, TITLE, firstVideo.Title)
	assert.Equal(t, DESCRIPTION, firstVideo.Description)
	assert.Equal(t, URL, firstVideo.URL)

}
